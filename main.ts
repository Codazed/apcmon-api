import express from 'express'
import execa from 'execa'
import moment, { Duration, Moment } from 'moment'

const app = express()

app.get('/', async (req, res) => {
    let responded = false
    console.log('Received request from ' + req.ip)
    GetUPSData().then(data => {
        res.send(data)
        console.log('Sent response to ' + req.ip)
        responded = true
    }).catch(err => {
        console.error(err.shortMessage)
    })
    setTimeout(() => {
        if (!responded) {
            res.status(502)
            res.send('An error occurred retrieving the UPS status')
        }
    }, 8000)
})

app.listen(3838)

async function GetUPSData(): Promise<UPSData> {
    const {stdout} = await execa('apcaccess')
    const parsedOne = stdout.split('\n')
    const parsedMap = new Map()

    parsedOne.forEach(item => {
        const parsedItem = item.split(/ *: /)
        parsedMap.set(parsedItem[0].trim(), parsedItem[1].trim())
    })
    
    const data: UPSData = {
        name: parsedMap.get('UPSNAME'),
        status: parsedMap.get('STATUS'),
        voltage: Number(parsedMap.get('LINEV').split(' ')[0]),
        load: parsedMap.get('LOADPCT').split(' ')[0]*0.01,
        battery: {
            charge: parsedMap.get('BCHARGE').split(' ')[0]*0.01,
            etl: moment.duration(parsedMap.get('TIMELEFT').split(' ')[0], parsedMap.get('TIMELEFT').split(' ')[1]),
            mbc: parsedMap.get('MBATTCHG').split(' ')[0]*0.01,
            mtl: moment.duration(parsedMap.get('MINTIMEL').split(' ')[0], parsedMap.get('MINTIMEL').split(' ')[1]),
            maxTime: moment.duration(parsedMap.get('MAXTIME').split(' ')[0], parsedMap.get('MAXTIME').split(' ')[1]),
            date: moment(parsedMap.get('BATTDATE'))
        }
    }
    return data
}

interface UPSData {
    name: String
    status: String
    voltage: number
    load: number
    battery: {
        charge: number
        etl: Duration
        mbc: number
        mtl: Duration
        maxTime: Duration
        date: Moment
    }
}